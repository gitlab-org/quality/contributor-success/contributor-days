---
title: "GitLab Contributor Days"

description: "Belgium, Friday, January 27th - Saturday, January 28th"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

[Coming soon!](https://gitlab.com/gitlab-org/quality/contributor-success/team-task/-/issues/63)
